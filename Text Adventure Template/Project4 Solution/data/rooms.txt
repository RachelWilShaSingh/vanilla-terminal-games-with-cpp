TOTAL_ROOMS		10

ROOM_BEGIN
NAME
Forest Clearing
DESCRIPTION
You are standing in a forest clearing.
ROOM_END

ROOM_BEGIN
NAME
Cave Entrance
DESCRIPTION
You are standing at the edge of the forest, near the entrance to a cave.
ROOM_END

ROOM_BEGIN
NAME
Cave Inner
DESCRIPTION
The cave is damp and dark and continues even further.
ROOM_END

ROOM_BEGIN
NAME
Forest River
DESCRIPTION
You've encountered a little river cutting through the forest.
ROOM_END

ROOM_BEGIN
NAME
Forest Pond
DESCRIPTION
The river ends in a small pond here, with various plants growing along the edge.
ROOM_END

ROOM_BEGIN
NAME
Forest Edge West
DESCRIPTION
The forest thins here.
ROOM_END

ROOM_BEGIN
NAME
Forest Edge East
DESCRIPTION
The forest clears and you can see an old house sitting to the south.
ROOM_END

ROOM_BEGIN
NAME
Abandoned House
DESCRIPTION
The abandoned house is musty and old. Its floorboards creek as you walk around.
ROOM_END

ROOM_BEGIN
NAME
Basement
DESCRIPTION
The house's basement is dark and to the north opens up to a cave system.
ROOM_END

ROOM_BEGIN
NAME
Attic
DESCRIPTION
The house's attic is full of old abandoned items, all covered in a layer of dust and mildew.
ROOM_END

NEIGHBORS
Forest Clearing
EAST
Cave Entrance

NEIGHBORS
Cave Entrance
EAST
Cave Inner

NEIGHBORS
Cave Inner
SOUTH
Basement

NEIGHBORS
Forest Clearing
SOUTH
Forest River

NEIGHBORS
Forest River
EAST
Forest Pond

NEIGHBORS
Forest River
SOUTH
Forest Edge West

NEIGHBORS
Forest Pond
SOUTH
Forest Edge East

NEIGHBORS
Forest Edge East
WEST
Forest Edge West

NEIGHBORS
Forest Edge East
SOUTH
Abandoned House

NEIGHBORS
Abandoned House
WEST
Attic

NEIGHBORS
Abandoned House
EAST
Basement
