#include "Game.hpp"

#include <iostream>
#include <iomanip>
using namespace std;

Game::Game()
{
    m_day = 1;
    m_done = false;
    m_locations.push_back( "Overland Park" );
    m_locations.push_back( "Raytown" );
    m_locations.push_back( "Kansas City" );
    m_locations.push_back( "Gladstone" );
}

void Game::Run()
{
    Menu_GameStart();

    while ( !m_done )
    {
        Menu_Main();
    }

    // Game over
    cout << endl << endl << "You survived the apocalpyse on your own for " << m_day << "days." << endl;
}

void Game::Menu_GameStart()
{
    cout << "Z O M B I E  -  A P O C A L Y P S E" << endl;
    cout << "Enter your name: ";
    getline( cin, m_player.name );
    cout << endl;
}

void Game::Menu_Main()
{
    DisplayMenu();
    int choice = GetChoice( 1, 2 ); // Scavenge or Travel

    if ( choice == 1 )          // Scavenge
    {
        Menu_Scavenge();
    }
    else if ( choice == 2 )     // Travel
    {
        Menu_Travel();
    }

    cout << endl << "Press ENTER to continue...";
    cin.ignore();
    string dump;
    getline( cin, dump );

    cout << "----------------------------------------" << endl;
    cout << "----------------------------------------" << endl;
}

void Game::Menu_Scavenge()
{
    bool actionSuccessful = true;
    cout << "* You scavenge here" << endl;

    int random = rand() % 5;
    RandomEvent_Scavenging( random );
}

void Game::RandomEvent_Scavenging( int event )
{
    int mod;
    if ( event == 0 )
    {
        mod = rand() % 3 + 2;
        cout << "* You find a stash of food. (+" << mod << " food)" << endl;
        m_player.food += mod;
    }
    else if ( event == 1 )
    {
        mod = rand() % 8 + 2;
        cout << "* A zombie surprises you!" << endl;
        cout << "* You get hurt in the encounter. (-" << mod << " health)" << endl;
        m_player.health -= mod;
    }
    else if ( event == 2 )
    {
        mod = rand() % 3 + 2;
        cout << "* You find some medical supplies. (+" << mod << " health)" << endl;
        m_player.health += mod;
    }
    else if ( event == 3 )
    {
        mod = rand() % 4 + 2;
        cout << "* Another scavenger ambushes you!" << endl;
        cout << "* They take some supplies from you. (-" << mod << " food)" << endl;
        m_player.food -= mod;
    }
    else if ( event == 4 )
    {
        cout << "* You don't find anything." << endl;
    }
}

void Game::Menu_Travel()
{
    cout << "Walk to where?" << endl;
    DisplayLocations();
    int choice = GetChoice( 1, 4 ) - 1;

    bool moved = false;

    if ( m_player.location == m_locations[choice] )
    {
        cout << "You're already there!" << endl;
    }
    else
    {
        m_player.location = m_locations[choice];
        moved = true;
    }

    if ( moved )
    {
        cout << endl << "* You travel to " << m_locations[choice] << "." << endl;

        int random = rand() % 5;
        RandomEvent_Moving( random );
    }
}

void Game::RandomEvent_Moving( int event )
{
    int mod;
    if ( event == 0 )
    {
        cout << "* A zombie attacks!" << endl;
        cout << "* You fight it off." << endl;
    }
    else if ( event == 1 )
    {
        mod = rand() % 8 + 2;
        cout << "* A zombie attacks!" << endl;
        cout << "* It bites you as you fight it! (-" << mod << " health)" << endl;
        m_player.health -= mod;
    }
    else if ( event == 2 )
    {
        mod = rand() % 3 + 2;
        cout << "* You find another scavenger and trade goods. (+" << mod << " food)" << endl;
        m_player.food += mod;
    }
    else if ( event == 3 )
    {
        mod = rand() % 4 + 2;
        cout << "* You find a safe building to rest in temporarily. (+" << mod << " health)" << endl;
        m_player.health += mod;
    }
    else if ( event == 4 )
    {
        cout << "* The journey is uneventful." << endl;
    }
}

void Game::EndOfDayStats()
{
    int mod;
    cout << "* The day passes (+1 day)." << endl;
    m_day++;

    if ( m_player.food > 0 )
    {
        cout << "* You eat a meal (-1 food)." << endl;
        m_player.food--;
        m_player.health++;
    }
    else
    {
        mod = rand() % 4 + 1;
        cout << "* You are starving! (-" << mod << " health)" << endl;
        m_player.health -= mod;
    }

    // Check for special states
    if ( m_player.health > 20 )
    {
        m_player.health = 20;
    }

    if ( m_player.food <= 0 )
    {
        m_player.food = 0;
    }

    if ( m_player.health <= 0 )
    {
        cout << "* You have died." << endl;
        m_done = true;
    }
    else if ( m_day >= 20 )
    {
        cout << endl;
        cout << "In the morning, a group of scavengers find you." << endl;
        cout << "They have a fortification nearby and are rebuilding a society." << endl;
        cout << endl << "You agree to live in their town." << endl;
        m_done = true;
    }

}

void Game::DisplayLocations()
{
    for ( size_t i = 0; i < m_locations.size(); i++ )
    {
        cout << (i+1) << ". " << m_locations[i] << endl;
    }
}

int Game::GetChoice( int min, int max )
{
    cout << "CHOICE (" << min << " - " << max << "): ";
    int choice;
    cin >> choice;

    while ( choice < min || choice > max )
    {
        cout << "INVALID SELECTION, TRY AGAIN: ";
        cin >> choice;
    }

    return choice;
}

void Game::DisplayMenu()
{
    cout << "----------------------------------------" << endl;
    cout << left << setw( 3 ) << "-- " << setw( 35 ) << m_player.name << "--" << endl;
    cout << left
        << setw( 6 ) << "-- Day "   << setw( 4 ) << m_day
        << setw( 6 ) << "Food: "    << setw( 4 ) << m_player.food
        << setw( 8 ) << "Health: "  << setw( 2 ) << m_player.health << setw( 1 )
        << "/" << setw( 2 ) << m_player.maxHealth
        << right << setw( 6 ) << "--" << endl;
    cout << left
        << setw( 10 ) << "-- Location: " << setw( 20 ) << m_player.location
        << right << setw( 7 ) << "--" << endl;
    cout << "----------------------------------------" << endl;
    cout << "-- 1. Scavenge here                   --" << endl;
    cout << "-- 2. Travel elseware                 --" << endl;
    cout << "----------------------------------------" << endl;
    for ( int i = 0; i < 15; i++ )
    {
        cout << endl;
    }
}
