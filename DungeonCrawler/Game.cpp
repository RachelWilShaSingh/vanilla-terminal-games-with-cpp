#include "Game.hpp"

#include <iostream>
using namespace std;

void Game::Run()
{
    m_map.CreateMap( 80, 15 );
    m_score = 0;
    m_status = "You entered the dungeon";

    bool done = false;
    while ( !done )
    {
        ClearScreen();
        m_map.Draw();
        PrintHud();

        char choice;
        cin >> choice;
        choice = tolower( choice );

        // Try to move in a direction
        if ( choice == 'a' )
        {
            m_map.TryToMove( "player", WEST );
            m_status = "You moved west";
        }
        else if ( choice == 'd' )
        {
            m_map.TryToMove( "player", EAST );
            m_status = "You moved east";
        }
        else if ( choice == 's' )
        {
            m_map.TryToMove( "player", SOUTH );
            m_status = "You moved south";
        }
        else if ( choice == 'w' )
        {
            m_map.TryToMove( "player", NORTH );
            m_status = "You moved north";
        }

        // Check if player has collected $
        if ( m_map.AreTheyTouching( "player", "bucks" ) )
        {
            m_score += 1;

            // New map
            m_map.CreateMap( 80, 15 );

            m_status = "You collected the bucks!";
        }

        // Check if enemy is touching player
        if ( m_map.AreTheyTouching( "player", "enemy1" ) || m_map.AreTheyTouching( "player", "enemy2" ) )
        {
            m_score -= 2;

            // New map
            m_map.CreateMap( 80, 15 );

            m_status = "You hit an enemy and were knocked out";
        }
    }
}

void Game::ClearScreen()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "cls" );
    #else
        system( "clear" );
    #endif
}

void Game::PrintHud()
{
    for ( int i = 0; i < m_map.Width(); i++ )
    {
        cout << "-";
    }
    cout << endl;
    cout << "SCORE: " << m_score << "\t\t" << m_status << endl;
    cout << "YOU ARE @ \t\t ENEMIES ARE x \t\t COLLECT $" << endl;
    cout << "MOVE WITH [W]   " << endl;
    cout << "       [A][S][D]   THEN HIT ENTER" << endl;
    cout << endl;
    cout << ">> ";
}
