#include "Game.hpp"

#include <cstdlib>
#include <ctime>

int main()
{
    // Initialize random numbers
    srand( time( NULL ) );

    // Start game
    Game game;
    game.Run();

    return 0;
}
