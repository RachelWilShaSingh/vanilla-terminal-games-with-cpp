#ifndef _GAME_HPP
#define _GAME_HPP

#include "Map.hpp"

class Game
{
    public:
    void Run();

    private:
    void PrintHud();
    void ClearScreen();

    Map m_map;
    int m_score;
    string m_status;
};

#endif
