#ifndef _MAP_HPP
#define _MAP_HPP

#include <vector>
#include <string>
using namespace std;

enum Direction
{
    NORTH,
    SOUTH,
    EAST,
    WEST
};

struct Room
{
    Room() { }
    Room( int x, int y )
    {
        this->x = x;
        this->y = y;
    }

    int x, y;
};

struct Object
{
    Object();
    Object( int x, int y, char symbol, string name )
    {
        this->x = x;
        this->y = y;
        this->symbol = symbol;
        this->name = name;
    }

    int x, y;
    char symbol;
    string name;
};

class Map
{
    public:
    Map();
    ~Map();

    void CreateMap( int width, int height );
    char Get( int x, int y );
    void Draw();

    void TryToMove( string name, Direction direction );
    bool AreTheyTouching( string name1, string name2 );

    int Width();
    int Height();

    private:
    char ** m_tiles;
    int m_width;
    int m_height;
    vector<Room> m_roomCenters;
    vector<Object> m_objects;

    void AllocateMemory( int width, int height );
    void DeallocateMemory();
    void GenerateMap();
};

#endif
