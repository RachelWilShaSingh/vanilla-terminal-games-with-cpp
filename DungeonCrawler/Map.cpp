#include "Map.hpp"

#include <cstdlib>

#include <vector>
#include <iostream>
using namespace std;

Map::Map()
{
    m_tiles = nullptr;
}

Map::~Map()
{
    DeallocateMemory();
}

void Map::CreateMap( int width, int height )
{
    AllocateMemory( width, height );
    GenerateMap();
}

char Map::Get( int x, int y )
{
    return m_tiles[x][y];
}

bool Map::AreTheyTouching( string name1, string name2 )
{
    int index1 = -1, index2 = -1;
    for ( unsigned int i = 0; i < m_objects.size(); i++ )
    {
        if ( m_objects[i].name == name1 )
        {
            index1 = i;
        }
        else if ( m_objects[i].name == name2 )
        {
            index2 = i;
        }
    }

    if ( index1 == -1 || index2 == -1 )
    {
        return false;
    }

    return ( m_objects[index1].x == m_objects[index2].x && m_objects[index1].y == m_objects[index2].y );
}

void Map::TryToMove( string name, Direction direction )
{
    // Locate the object with this name
    int objectIndex = -1;
    for ( unsigned int i = 0; i < m_objects.size(); i++ )
    {
        if ( m_objects[i].name == name )
        {
            objectIndex = i;
            break;
        }
    }

    if ( objectIndex == -1 )
    {
        // Invalid object
        return;
    }

    // Where is the object going?
    int desiredX = m_objects[objectIndex].x;
    int desiredY = m_objects[objectIndex].y;

    if ( direction == NORTH )
    {
        desiredY--;
    }
    else if ( direction == SOUTH )
    {
        desiredY++;
    }
    else if ( direction == EAST )
    {
        desiredX++;
    }
    else if ( direction == WEST )
    {
        desiredX--;
    }

    bool invalidPath = false;

    // Is the direction they want to go out-of-bounds?
    if ( desiredX < 0 || desiredX >= m_width ||
        desiredY < 0 || desiredY >= m_height )
    {
        invalidPath = true;
    }

    // Is there a wall in the direction they want to go?
    else if ( m_tiles[desiredX][desiredY] == '#' )
    {
        invalidPath = true;
    }

    if ( !invalidPath )
    {
        // It's not invalid, we can move there.
        m_objects[objectIndex].x = desiredX;
        m_objects[objectIndex].y = desiredY;
    }
}

int Map::Width()
{
    return m_width;
}

int Map::Height()
{
    return m_height;
}

void Map::Draw()
{
    for ( int y = 0; y < m_height; y++ )
    {
        for ( int x = 0; x < m_width; x++ )
        {
            // Check to see if there is an enemy or player here.
            bool objPresent = false;
            for ( auto& obj : m_objects )
            {
                if ( obj.x == x && obj.y == y )
                {
                    objPresent = true;
                    cout << obj.symbol;
                }
            }

            // Draw an empty spot if no objects are in this space.
            if ( !objPresent )
            {
                cout << m_tiles[x][y];
            }
        }
        cout << endl;
    }
}

void Map::AllocateMemory( int width, int height )
{
    // Set dimensions
    m_width = width;
    m_height = height;

    // Allocate outer array
    m_tiles = new char * [m_width];

    // Allocate inner arrays
    for ( int i = 0; i < m_width; i++ )
    {
        m_tiles[i] = new char[width];
    }
}

void Map::DeallocateMemory()
{
    if ( m_tiles != nullptr )
    {
        // Free inner arrays
        for ( int i = 0; i < m_width; i++ )
        {
            if ( m_tiles[i] != nullptr )
            {
                delete [] m_tiles[i];
            }
        }

        // Free outer array
        delete [] m_tiles;
    }
}

void Map::GenerateMap()
{
    m_objects.clear();

    // Initialize map
    for ( int y = 0; y < m_height; y++ )
    {
        for ( int x = 0; x < m_width; x++ )
        {
            m_tiles[x][y] = '#';
        }
    }

    // Choose an amount of rooms.
    int roomCount = rand() % 8 + 4;

    vector<int> roomX;
    vector<int> roomY;
    for ( int room = 0; room < roomCount; room++ )
    {
        // Get random coordinates
        int x = rand() % m_width;
        int y = rand() % m_height;

        // A walkable room is an empty space
        m_tiles[x][y] = ' ';

        // How big is the room?
        int wOffset = rand() % 3 + 2;
        int hOffset = rand() % 2 + 2;

        for ( int x2 = x - wOffset; x2 <= x + wOffset; x2++ )
        {
            for ( int y2 = y - hOffset; y2 < y + hOffset; y2++ )
            {
                if ( x2 < 0 || y2 < 0 || x2 >= m_width || y2 >= m_height )
                {
                    // Skip if it is out of bounds.
                    continue;
                }
                m_tiles[x2][y2] = ' ';
            }
        }

        // Keep track of our room center points
        m_roomCenters.push_back( Room( x, y ) );
    }

    // Build paths between each room
    for ( unsigned int a = 0; a < m_roomCenters.size() - 1; a++ )
    {
        int roomAX = m_roomCenters[a].x;
        int roomAY = m_roomCenters[a].y;
        int roomBX = m_roomCenters[a+1].x;
        int roomBY = m_roomCenters[a+1].y;

        // Make a path between them
        int x = roomAX;
        int y = roomAY;

        while ( x != roomBX || y != roomBY )
        {
            // Make a path
            m_tiles[x][y] = ' ';

            // Traverse to next tile
            if      ( x < roomBX ) { x++; }
            else if ( x > roomBX ) { x--; }
            else if ( y < roomBY ) { y++; }
            else if ( y > roomBY ) { y--; }
        }
    }

    // Add player
    m_objects.push_back( Object( m_roomCenters[0].x, m_roomCenters[0].y, '@', "player" ) );

    // Add enemies
    m_objects.push_back( Object( m_roomCenters[1].x, m_roomCenters[1].y, 'x', "enemy1" ) );
    m_objects.push_back( Object( m_roomCenters[2].x, m_roomCenters[2].y, 'x', "enemy2" ) );

    // Add bucks
    m_objects.push_back( Object( m_roomCenters[3].x, m_roomCenters[3].y, '$', "bucks" ) );
}

