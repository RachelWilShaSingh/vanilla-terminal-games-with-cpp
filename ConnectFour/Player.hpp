#ifndef _PLAYER_HPP
#define _PLAYER_HPP

#include <string>
using namespace std;

struct Player
{
    string name = "UNSET";
    char symbol = 'R';
    int wins = 0;
};

#endif
