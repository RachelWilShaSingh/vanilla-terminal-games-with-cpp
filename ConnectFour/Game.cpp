#include "Game.hpp"

#include "utilities/ScreenDrawer.hpp"
#include "utilities/Helper.hpp"

#include <iostream>
using namespace std;

void Game::Run()
{
    Init();
    Play();
}

void Game::Init()
{
    m_board.Clear();

    m_players[0].name = "Player 1";
    m_players[0].symbol = '1';

    m_players[1].name = "Player 2";
    m_players[1].symbol = '2';
}

void Game::Play()
{
    bool done = false;
    int rounds = 1;
    int turn = 0;
    int winner = -1;
    while ( !done )
    {
        ScreenDrawer::Clear();

        m_board.Draw();

        ScreenDrawer::Set( 0, 10, "TURN: Player " + Helper::ToString( turn+1 ) );
        ScreenDrawer::Set( 0, 11, "ENTER AN X COORDINATE TO PLACE YOUR PIECE." );

        ScreenDrawer::Draw();

        int x = Helper::GetIntInput( 0, m_board.GetWidth()-1 );

        while ( m_board.AddPiece( x, m_players[turn].symbol ) == false )
        {
            cout << "NOT ENOUGH SPACE IN THAT COLUMN. TRY AGAIN." << endl;
            x = Helper::GetIntInput( 0, m_board.GetWidth()-1 );
        }

        char winner = m_board.CheckForWinner();
        if ( winner == m_players[0].symbol )
        {
            done = true;
            winner = 0;
        }
        else if ( winner == m_players[1].symbol )
        {
            done = true;
            winner = 1;
        }

        if ( turn == 0 )    { turn = 1; }
        else                { turn = 0; }
    }

    cout << m_players[winner].name << " WINS!" << endl;
}
